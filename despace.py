#!/usr/bin/python
#-*- coding: utf-8 -*-

# agrell@havi.de, 2014

######################################################
# Das vorliegende Skript wandelt störende            #
# Leerzeichen in Dateinamen im aktuellen Verzeichnis #
# in Unterstriche um.                                #
######################################################

import os
from shutil import move 

dateien = [f for f in os.listdir('.') if os.path.isfile(f)]

for datei in dateien:
    print(f"Normalisisiere {datei}\n")
    name = datei.replace(" ", "_")
    print(f"Neuer Name: {name}")
    move(datei, name)
